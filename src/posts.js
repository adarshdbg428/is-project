const posts = [
  {
    id: "a",
    title: "Hi i am adarsh kumar from NITK",
    ownerId: ""
  },
  {
    id: "b",
    title: "Hi i am sayan vishwas from NITK",
    ownerId: "auth0|5b9835ccf1642721ad85bf93"
  },
  {
    id: "c",
    title: "Hi wait a minute,who are you?",
    ownerId: ""
  },
  {
    id: "d",
    title: "Always deliver more than expected.",
    ownerId: "auth0|5b9835ccf1642721ad85bf93"
  },
  {
    id: "e",
    title: "The most courageous act is still to think for yourself.",
    ownerId: "auth0|5b9835ccf1642721ad85bf93"
  },
  {
    id: "f",
    title: "What would you do if you were not afraid?",
    ownerId: "auth0|5ede4347bfcca300197e5d19"
  },
  {
    id: "g",
    title: "Nothing will work unless you do.",
    ownerId: "auth0|5b9835ccf1642721ad85bf93"
  },
  {
    id: "h",
    title: "Don't be intimidated by what you don't know.",
    ownerId: "auth0|5ede4347bfcca300197e5d19"
  }
];

export default posts;
